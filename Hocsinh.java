package danhsachquanlihosohocsinh;

public class Hocsinh {
public String name;
public int age;
public int classs;

public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getAge() {
	return age;
}

public void setAge(int age) {
	this.age = age;
}



public int getClasss() {
	return classs;
}

public void setClasss(int classs) {
	this.classs = classs;
}

public Hocsinh (String name, int age, int classs) {
	this.setName(name);
	this.setAge(age);
	this.setClasss(classs);
}
public String toString() {
    return "Student " +"\n"+ "name:" + name +"\n" + "age:" + age + "\n"+ "class:" + classs;
}

}
