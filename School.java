package danhsachquanlihosohocsinh;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class School {
	 private List<Hocsinh> students;

	    public School() {
	        this.students = new ArrayList<>();
	    }

	    public void add(Hocsinh student) {
	        this.students.add(student);
	    }
	    public List<Hocsinh> getStudent20YearOld() {
	        return this.students.stream().filter(student -> student.getAge() == 20).collect(Collectors.toList());
	    }

	   

}
